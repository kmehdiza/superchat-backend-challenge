package com.superchat.customer.config;

import com.superchat.common.config.ModelMapperConfig;
import com.superchat.common.exception.GlobalExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import({ModelMapperConfig.class, GlobalExceptionHandler.class})
@Configuration
public class Config {
}
