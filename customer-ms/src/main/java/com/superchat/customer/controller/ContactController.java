package com.superchat.customer.controller;

import com.superchat.customer.dto.ContactRequestDto;
import com.superchat.customer.dto.ContactResponseDto;
import com.superchat.customer.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/contacts")
public class ContactController {

    private final ContactService contactService;

    @PostMapping
    public ResponseEntity<ContactResponseDto> create(@RequestBody @Valid ContactRequestDto requestDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(contactService.createContact(requestDto));
    }

    @GetMapping("/all")
    public ResponseEntity<List<ContactResponseDto>> getAll() {
        return ResponseEntity.ok(contactService.findAllContacts());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ContactResponseDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(contactService.getContact(id));
    }
}
