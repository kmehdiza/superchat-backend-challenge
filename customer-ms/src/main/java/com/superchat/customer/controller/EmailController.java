package com.superchat.customer.controller;

import com.superchat.customer.dto.UserDto;
import com.superchat.customer.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/email")
public class EmailController {

    private final EmailService emailService;

    @PostMapping("/payment-verification")
    public void paymentVerificationEmailSender(@RequestBody UserDto userDto) {
        emailService.paymentVerificationMailSender(userDto);
    }
}
