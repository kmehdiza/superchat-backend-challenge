package com.superchat.customer.controller;

import com.superchat.customer.dto.IdDto;
import com.superchat.customer.dto.MessageRequestDto;
import com.superchat.customer.dto.MessageResponseDto;
import com.superchat.customer.dto.MessageUpdateDto;
import com.superchat.customer.service.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/messages")
public class MessageController {

    private final MessageService messageService;

    @PostMapping
    public ResponseEntity<MessageResponseDto> sendMessage(@RequestBody @Valid MessageRequestDto requestDto) {
        return ResponseEntity.ok(messageService.sendMessage(requestDto));
    }

    @PutMapping
    public ResponseEntity<MessageResponseDto> updateMessage(@RequestBody @Valid MessageUpdateDto updateDto) {
        return ResponseEntity.ok(messageService.updateMessage(updateDto));
    }

    @GetMapping("/all/history")
    public ResponseEntity<List<MessageResponseDto>> getAllHistory(@RequestBody IdDto dto) {
        return ResponseEntity.ok(messageService.getAllHistory(dto));
    }
}
