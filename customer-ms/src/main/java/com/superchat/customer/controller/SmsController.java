package com.superchat.customer.controller;

import com.superchat.customer.dto.SmsDto;
import com.superchat.customer.service.SmsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sms")
public class SmsController {

    private final SmsService smsService;

    @PostMapping("/send")
    public void receiveSms(@RequestBody SmsDto sms) {
        smsService.sendSms(sms);
    }
}
