package com.superchat.customer.exception;

import com.superchat.common.exception.NotFoundException;

public class ContactNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 5843213248811L;

    private static final String MESSAGE = "Contact id: %s does not exist";

    public ContactNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }
}
