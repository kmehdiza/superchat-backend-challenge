package com.superchat.customer.exception;

import com.superchat.common.exception.InvalidStateException;

public class EmailAlreadyExistException extends InvalidStateException {

    private static final long serialVersionUID = 5843213248811L;

    public static final String MESSAGE = "Contact Email: %s already exist.";

    public EmailAlreadyExistException(String email) {
        super(String.format(MESSAGE,email));
    }
}
