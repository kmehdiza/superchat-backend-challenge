package com.superchat.customer.exception;

import com.superchat.common.exception.NotFoundException;

public class EmailNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 5843213248811L;

    private static final String MESSAGE = "Email %s does not exist";

    public EmailNotFoundException(String email) {
        super(String.format(MESSAGE, email));
    }
}
