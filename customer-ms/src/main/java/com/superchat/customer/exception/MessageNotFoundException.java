package com.superchat.customer.exception;

import com.superchat.common.exception.NotFoundException;

public class MessageNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 5843213248811L;

    private static final String MESSAGE = "Message id: %s does not exist";

    public MessageNotFoundException(Long id) {
        super(String.format(MESSAGE,id));
    }
}
