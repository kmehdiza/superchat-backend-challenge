package com.superchat.customer.module.enums;

public enum MessageType {
    SENT, UPDATED
}
