package com.superchat.customer.service;

import com.superchat.customer.dto.ContactRequestDto;
import com.superchat.customer.dto.ContactResponseDto;

import java.util.List;

public interface ContactService {
    ContactResponseDto createContact(ContactRequestDto contactRequestDto);

    List<ContactResponseDto> findAllContacts();

    ContactResponseDto getContact(Long id);
}
