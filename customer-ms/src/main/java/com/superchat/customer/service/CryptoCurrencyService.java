package com.superchat.customer.service;

import com.superchat.customer.dto.BitcoinDto;

public interface CryptoCurrencyService {
    BitcoinDto getBitcoinData();
}
