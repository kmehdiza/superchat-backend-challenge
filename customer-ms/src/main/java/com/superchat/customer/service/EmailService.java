package com.superchat.customer.service;

import com.superchat.customer.dto.UserDto;

public interface EmailService {
    void paymentVerificationMailSender(UserDto userDto);
}
