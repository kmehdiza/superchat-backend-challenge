package com.superchat.customer.service;

import com.superchat.customer.dto.IdDto;
import com.superchat.customer.dto.MessageRequestDto;
import com.superchat.customer.dto.MessageResponseDto;
import com.superchat.customer.dto.MessageUpdateDto;
import java.util.List;

public interface MessageService {

    MessageResponseDto sendMessage(MessageRequestDto messageRequestDto);

    MessageResponseDto updateMessage(MessageUpdateDto messageUpdateDto);

    List<MessageResponseDto> getAllHistory(IdDto dto);

}
