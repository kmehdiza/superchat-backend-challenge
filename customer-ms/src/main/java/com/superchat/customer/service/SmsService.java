package com.superchat.customer.service;

import com.superchat.customer.dto.SmsDto;

public interface SmsService {
    void sendSms(SmsDto sms);
}
