package com.superchat.customer.service.impl;

import com.superchat.customer.dto.ContactRequestDto;
import com.superchat.customer.dto.ContactResponseDto;
import com.superchat.customer.exception.ContactNotFoundException;
import com.superchat.customer.exception.EmailAlreadyExistException;
import com.superchat.customer.module.Contact;
import com.superchat.customer.repository.ContactRepository;
import com.superchat.customer.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ContactServiceImpl implements ContactService {

    private final ContactRepository contactRepository;
    private final ModelMapper modelMapper;

    @Override
    public ContactResponseDto createContact(ContactRequestDto contactRequestDto) {
        checkIfEmailExist(contactRequestDto.getEmail());
        return modelMapper.map(contactRepository.save(modelMapper
                .map(contactRequestDto, Contact.class)), ContactResponseDto.class);
    }

    @Override
    public List<ContactResponseDto> findAllContacts() {
        return contactRepository.findAll().stream()
                .map(contact -> modelMapper.map(contact, ContactResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public ContactResponseDto getContact(Long id) {
        return contactRepository.findById(id)
                .map(contact -> modelMapper.map(contact, ContactResponseDto.class))
                .orElseThrow(() -> new ContactNotFoundException(id));
    }

    private void checkIfEmailExist(String email) {
        contactRepository.findByEmail(email).ifPresent(contact -> {
            throw new EmailAlreadyExistException(contact.getEmail());
        });
    }
}
