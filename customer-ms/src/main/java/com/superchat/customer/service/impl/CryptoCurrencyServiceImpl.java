package com.superchat.customer.service.impl;

import com.google.gson.Gson;
import com.superchat.customer.util.HttpUtil;
import com.superchat.customer.dto.BitcoinDto;
import com.superchat.customer.service.CryptoCurrencyService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CryptoCurrencyServiceImpl implements CryptoCurrencyService {

    private final HttpUtil httpUtil;
    private final Gson gson;

    private static final String BPI_JSON = "bpi";
    private static final String USD_JSON = "USD";

    @Override
    @SneakyThrows
    public BitcoinDto getBitcoinData() {
        return parseJsonToDto(httpUtil.getRequest());
    }

    @SneakyThrows
    private BitcoinDto parseJsonToDto(String data) {
        JSONObject bpi = (JSONObject) getJsonValue(BPI_JSON, parseStringToJson(data)).get(USD_JSON);
        return gson.fromJson(String.valueOf(bpi), BitcoinDto.class);
    }

    @SneakyThrows
    private JSONObject parseStringToJson(String data) {
        return (JSONObject) new JSONParser().parse(data);
    }

    private JSONObject getJsonValue(String key, JSONObject jsonObject) {
        return (JSONObject) jsonObject.get(key);
    }
}
