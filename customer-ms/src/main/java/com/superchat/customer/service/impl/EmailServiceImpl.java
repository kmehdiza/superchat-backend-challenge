package com.superchat.customer.service.impl;

import com.superchat.customer.dto.BitcoinDto;
import com.superchat.customer.dto.UserDto;
import com.superchat.customer.service.CryptoCurrencyService;
import com.superchat.customer.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;
    private final CryptoCurrencyService cryptoCurrencyService;

    @Value("${spring.mail.username}")
    private String hostMail;

    private static final String SUBJECT = "Payment verification";
    private static final String PAYMENT_VERIFICATION_TEXT = "Hello %s, " +
            "\n\nThank you for the cooperation, you have made successful transaction . " +
            "\n\nIn case, if you are interested in the today's hot topic, it would worth to check it :)" +
            "\n\nThe recent Price of Bitcoin is %s %s: ";

    @Override
    public void paymentVerificationMailSender(UserDto userDto) {
        javaMailSender.send(createSimpleMailMessage(userDto, cryptoCurrencyService.getBitcoinData()));
    }

    private SimpleMailMessage createSimpleMailMessage(UserDto userDto, BitcoinDto bitcoinDto) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(hostMail);
        message.setTo(userDto.getEmail());
        message.setSubject(SUBJECT);
        message.setText(typePaymentVerificationEmail(userDto, bitcoinDto));
        return message;
    }

    private String typePaymentVerificationEmail(UserDto userDto, BitcoinDto bitcoinDto) {
        return String.format(PAYMENT_VERIFICATION_TEXT, userDto.getName(), bitcoinDto.getRate(), bitcoinDto.getCode());
    }
}
