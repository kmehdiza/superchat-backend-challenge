package com.superchat.customer.service.impl;

import com.superchat.customer.dto.IdDto;
import com.superchat.customer.dto.MessageRequestDto;
import com.superchat.customer.dto.MessageResponseDto;
import com.superchat.customer.dto.MessageUpdateDto;
import com.superchat.customer.exception.EmailNotFoundException;
import com.superchat.customer.exception.MessageNotFoundException;
import com.superchat.customer.module.Contact;
import com.superchat.customer.module.Message;
import com.superchat.customer.module.enums.MessageType;
import com.superchat.customer.repository.ContactRepository;
import com.superchat.customer.repository.MessageRepository;
import com.superchat.customer.service.MessageService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;
    private final ContactRepository contactRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public MessageResponseDto sendMessage(MessageRequestDto messageRequestDto) {
        Contact contact = findContactByEmail(messageRequestDto.getEmail());
        Message message = modelMapper.map(messageRequestDto, Message.class);
        message.setContact(contact);
        message.setMessageType(MessageType.SENT.toString());
        return modelMapper.map(messageRepository.save(message), MessageResponseDto.class);
    }

    @Override
    public MessageResponseDto updateMessage(MessageUpdateDto messageUpdateDto) {
        return messageRepository.findById(messageUpdateDto.getId())
                .map(message -> {
                    message.setMessageBody(messageUpdateDto.getMessageBody());
                    message.setMessageType(MessageType.UPDATED.toString());
                    return modelMapper.map(messageRepository.save(message), MessageResponseDto.class);
                })
                .orElseThrow(() -> new MessageNotFoundException(messageUpdateDto.getId()));
    }

    @Override
    public List<MessageResponseDto> getAllHistory(IdDto dto) {
        return messageRepository.findAllById(dto.getIds()).stream()
                .map(message -> modelMapper.map(message, MessageResponseDto.class))
                .collect(Collectors.toList());
    }

    private Contact findContactByEmail(String email) {
        return contactRepository.findByEmail(email)
                .orElseThrow(() -> new EmailNotFoundException(email));
    }
}
