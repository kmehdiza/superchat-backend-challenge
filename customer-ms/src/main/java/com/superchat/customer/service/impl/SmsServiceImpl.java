package com.superchat.customer.service.impl;

import com.superchat.customer.dto.SmsDto;
import com.superchat.customer.service.SmsService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SmsServiceImpl implements SmsService {

    @Value("${twilio.accountId}")
    private String accountId;

    @Value("${twilio.token}")
    private String token;

    @Value("${twilio.fromNumber}")
    private String fromNumber;

    @Override
    public void sendSms(SmsDto sms) {
        Twilio.init(accountId, token);
        Message.creator(new PhoneNumber(sms.getNumber()), new PhoneNumber(fromNumber), sms.getMessage())
                .create();
    }
}
