package com.superchat.customer.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class HttpUtil {

    private final HttpClient client = HttpClient.newHttpClient();

    @Value("${currency.currentPrice.bitcoin.url}")
    private String bitcoinUrl;

    public String getRequest() throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .header("accept","application/json")
                .uri(URI.create(bitcoinUrl))
                .build();
        HttpResponse<String> send = client.send(request, HttpResponse.BodyHandlers.ofString());
        return send.body();
    }
}
