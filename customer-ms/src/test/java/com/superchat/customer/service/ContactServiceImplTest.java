package com.superchat.customer.service;

import com.superchat.customer.dto.ContactRequestDto;
import com.superchat.customer.dto.ContactResponseDto;
import com.superchat.customer.exception.ContactNotFoundException;
import com.superchat.customer.exception.EmailAlreadyExistException;
import com.superchat.customer.module.Contact;
import com.superchat.customer.repository.ContactRepository;
import com.superchat.customer.service.impl.ContactServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ContactServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";

    @InjectMocks
    private ContactServiceImpl contactService;

    @Mock
    private ContactRepository contactRepository;

    @Spy
    private ModelMapper modelMapper;

    private Contact contact;
    private ContactRequestDto contactRequestDto;
    private ContactResponseDto contactResponseDto;

    @BeforeEach
    void setUp() {
        contact = getContact();
        contactRequestDto = getContactRequestDto();
        contactResponseDto = getContactResponseDto();
    }

    @Test
    void givenEmailAlreadyExistWhenCreateContactThenException() {
        //Arrange
        when(contactRepository.findByEmail(DUMMY_STRING)).thenReturn(Optional.of(contact));

        //Act & Assert
        assertThatThrownBy(() -> contactService.createContact(contactRequestDto))
                .isInstanceOf(EmailAlreadyExistException.class);

    }

    @Test
    void givenCorrectParamsWhenCreateContactThenReturnContactResponseDto() {
        //Arrange
        when(contactRepository.findByEmail(DUMMY_STRING)).thenReturn(Optional.empty());
        when(contactRepository.save(contact)).thenReturn(contact);
        modelMapper.map(contact,ContactResponseDto.class);

        //Act
        ContactResponseDto result = contactService.createContact(contactRequestDto);

        //Assert
        verify(contactRepository, times(1)).save(contact);
        assertThat(result).isEqualTo(contactResponseDto);
    }

    @Test
    void givenNoParamsWhenFindAllContactThenReturnListOfContactResponseDto() {
        //Arrange
        when(contactRepository.findAll()).thenReturn(List.of(contact));

        //Act
        List<ContactResponseDto> listResponseDto = contactService.findAllContacts();

        //Assert
        assertThat(listResponseDto).isEqualTo(List.of(contactResponseDto));
    }

    @Test
    void givenIdIsNotPresentWhenGetContactThenException() {
        //Arrange
        when(contactRepository.findById(DUMMY_ID)).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> contactService.getContact(DUMMY_ID)).isInstanceOf(ContactNotFoundException.class);
    }

    @Test
    void givenIdIsPresentWhenGetContactThenReturnContactResponseDto() {
        //Arrange
        when(contactRepository.findById(DUMMY_ID)).thenReturn(Optional.of(contact));

        //Act
        ContactResponseDto result = contactService.getContact(DUMMY_ID);

        //Assert
        assertThat(result).isEqualTo(contactResponseDto);
    }

    private ContactRequestDto getContactRequestDto() {
        return ContactRequestDto
                .builder()
                .name(DUMMY_STRING)
                .surname(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }

    private ContactResponseDto getContactResponseDto() {
        return ContactResponseDto
                .builder()
                .name(DUMMY_STRING)
                .surname(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }

    private Contact getContact() {
        return Contact
                .builder()
                .name(DUMMY_STRING)
                .surname(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }
}
