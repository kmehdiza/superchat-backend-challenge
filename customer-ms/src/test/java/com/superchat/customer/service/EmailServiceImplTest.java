package com.superchat.customer.service;

import com.superchat.customer.dto.BitcoinDto;
import com.superchat.customer.dto.UserDto;
import com.superchat.customer.service.impl.CryptoCurrencyServiceImpl;
import com.superchat.customer.service.impl.EmailServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EmailServiceImplTest {

    private static final String DUMMY_STRING = "string";
    private static final Float DUMMY_FLOAT = 1.0f;

    @InjectMocks
    private EmailServiceImpl sendEmailService;

    @Mock
    private JavaMailSender javaMailSender;

    @Mock
    private CryptoCurrencyServiceImpl cryptoCurrencyService;

    private UserDto userDto;
    private BitcoinDto bitcoinDto;

    @BeforeEach
    void setUp() {
        userDto = getUserDto();
        bitcoinDto = getBitcoinDto();
    }

    @Test
    void givenUserDtoWhenPaymentVerificationMailSenderThenSuccess() {
        //Arrange
        when(cryptoCurrencyService.getBitcoinData()).thenReturn(bitcoinDto);
        doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));

        //Act
        sendEmailService.paymentVerificationMailSender(userDto);

        //Assert
        verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));

    }

    private UserDto getUserDto() {
        return UserDto
                .builder()
                .name(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }

    private BitcoinDto getBitcoinDto() {
        return BitcoinDto
                .builder()
                .code(DUMMY_STRING)
                .description(DUMMY_STRING)
                .rate(DUMMY_STRING)
                .symbol(DUMMY_STRING)
                .rateFloat(DUMMY_FLOAT)
                .build();
    }
}
