package com.superchat.customer.service;

import com.superchat.customer.dto.IdDto;
import com.superchat.customer.dto.MessageRequestDto;
import com.superchat.customer.dto.MessageResponseDto;
import com.superchat.customer.dto.MessageUpdateDto;
import com.superchat.customer.exception.EmailNotFoundException;
import com.superchat.customer.exception.MessageNotFoundException;
import com.superchat.customer.module.Contact;
import com.superchat.customer.module.Message;
import com.superchat.customer.module.enums.MessageType;
import com.superchat.customer.repository.ContactRepository;
import com.superchat.customer.repository.MessageRepository;
import com.superchat.customer.service.impl.MessageServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MessageServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";


    @InjectMocks
    private MessageServiceImpl messageService;

    @Mock
    private MessageRepository messageRepository;

    @Mock
    private ContactRepository contactRepository;

    @Spy
    private ModelMapper modelMapper;

    private Message message;
    private Contact contact;
    private IdDto idDto;
    private MessageRequestDto messageRequestDto;
    private MessageResponseDto messageResponseDto;
    private MessageUpdateDto messageUpdateDto;

    @BeforeEach
    void setUp() {
        contact = getContact();
        message = getMessage();
        idDto = getIdDto();
        messageRequestDto = getMessageRequestDto();
        messageResponseDto = getMessageResponseDto();
        messageUpdateDto = getMessageUpdateDto();
    }

    @Test
    void givenEmailIsNotPresentWhenSendMessageThenException() {
        //Arrange
        when(contactRepository.findByEmail(DUMMY_STRING)).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> messageService.sendMessage(messageRequestDto))
                .isInstanceOf(EmailNotFoundException.class);
    }

    @Test
    void givenMessageRequestDtoWhenSendMessageThenReturnMessageResponseDto() {
        //Arrange
        when(contactRepository.findByEmail(DUMMY_STRING)).thenReturn(Optional.of(contact));
        when(messageRepository.save(message)).thenReturn(message);

        //Act
        MessageResponseDto result = messageService.sendMessage(messageRequestDto);

        //Assert
        assertThat(result).isEqualTo(messageResponseDto);
    }

    @Test
    void givenIdIsNotPresentWhenUpdateMessageThenReturnException() {
        //Arrange
        when(messageRepository.findById(DUMMY_ID)).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> messageService.updateMessage(messageUpdateDto))
                .isInstanceOf(MessageNotFoundException.class);
    }

    @Test
    void givenMessageUpdateDtoWhenUpdateMessageThenReturnMessageResponseDto() {
        //Arrange
        when(messageRepository.findById(DUMMY_ID)).thenReturn(Optional.of(message));
        message.setMessageType(MessageType.UPDATED.toString());
        when(messageRepository.save(message)).thenReturn(message);
        MessageResponseDto responseDto = modelMapper.map(message, MessageResponseDto.class);

        //Act
        MessageResponseDto result = messageService.updateMessage(messageUpdateDto);
        assertThat(result).isEqualTo(responseDto);
    }

    @Test
    void givenValidIDsWhenGetAllHistoryThenReturnListOfMessageResponseDto() {
        //Arrange
        when(messageRepository.findAllById(idDto.getIds())).thenReturn(List.of(message));

        //Act
        List<MessageResponseDto> result = messageService.getAllHistory(idDto);

        //Assert
        assertThat(result).isEqualTo(List.of(messageResponseDto));
    }

    private IdDto getIdDto() {
        return IdDto
                .builder()
                .ids(List.of(DUMMY_ID))
                .build();
    }

    private MessageRequestDto getMessageRequestDto() {
        return MessageRequestDto
                .builder()
                .email(DUMMY_STRING)
                .messageBody(DUMMY_STRING)
                .build();
    }

    private MessageResponseDto getMessageResponseDto() {
        return MessageResponseDto
                .builder()
                .messageType(MessageType.SENT)
                .build();
    }

    private MessageUpdateDto getMessageUpdateDto() {
        return MessageUpdateDto
                .builder()
                .id(DUMMY_ID)
                .email(DUMMY_STRING)
                .messageBody(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }

    private Contact getContact() {
        return Contact
                .builder()
                .name(DUMMY_STRING)
                .surname(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }

    private Message getMessage() {
        return Message
                .builder()
                .messageBody(DUMMY_STRING)
                .messageType(MessageType.SENT.toString())
                .contact(contact)
                .build();
    }
}
